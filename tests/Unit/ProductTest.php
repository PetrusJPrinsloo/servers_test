<?php

namespace Tests\Unit;
namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;
use const Grpc\STATUS_OK;

/**
 * ToDo: create the rest of the tests
 *
 * Class ProductTest
 * @package Tests\Feature
 */
class ProductTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    /**
     * Test getting a list of products.
     *
     * @return void
     */
    public function testGetProducts()
    {
        $response = $this->json('GET', '/product');
        $response->assertStatus(200);
    }
}
