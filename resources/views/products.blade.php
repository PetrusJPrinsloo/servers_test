<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->
    <script src="https://kit.fontawesome.com/1d6c28f39e.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{mix('css/app.css')}}">

    <title>@lang('products.title')</title>
    <link rel="shortcut icon" href="{{ @url('img/favicon.ico') }}">

</head>

<body>

{{--Header--}}
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/product">
        <span class="fas fa-server"></span>
        @lang('products.title')
    </a>
</nav>

{{--Body--}}
<div class="container" id="app">
    <div class="row">
        <div>
            &nbsp;
        </div>
    </div>
    <div id="app">

        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <filter-products></filter-products>

                </div>
                <div class="col-md-7">
                    <product-list></product-list>

                </div>
            </div>
        </div>

    </div>
</div>

{{-- Footer --}}
<footer class="page-footer font-small pt-4">
    <div class="card  text-white bg-dark text-center">
        <div class="card-header">
            Created by Petrus J Prinsloo
        </div>
        <div class="card-body">
            <h5 class="card-title">Note</h5>
            <p class="card-text">This website is not real and has been created as a demonstration.</p>
        </div>
    </div>
</footer>

</body>

<script src="js/app.js" charset="utf-8">
</script>

</html>
