<?php
return [
    'instructions' => 'Select the filters and press filter to apply them',
    'storage' => 'Storage',
    'memory' => 'RAM',
    'storage_type' => 'Hard disk type',
    'location' => 'Location',
    'apply' => 'Apply',
    'title' => 'Products'
];
