let actions = {
    fetchProducts({commit}, product) {
        let url = '';

        if (product === undefined) {
            url = '/api/product';
        } else {
            url = '/api/product?' +
                'storage_type=' + product.storage_type +
                '&memory=' + product.memory.join(',') +
                '&location=' + product.location +
                '&storage=' + product.storage;
        }

        axios.get(url)
            .then(res => {
                commit('FETCH_PRODUCTS', res.data);
            }).catch(err => {
            console.log(err);
        })
    },
};

export default actions;
