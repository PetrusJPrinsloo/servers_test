let mutations = {
    FETCH_PRODUCTS(state, products) {
        return state.products = products;
    },

};

export default mutations;
