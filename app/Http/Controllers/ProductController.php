<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        /** @var string $memory */
        $memory = $request->has('memory')
            ? explode(',', $request->get('memory'))
            : [0,4,8,16,32,64,96,128];

        /** @var string $storageType */
        $storageType = $request->has('storage_type')
            ? explode(',', $request->get('storage_type'))
            : ['SSD', 'SATA', 'SAS'];

        /** @var int $storageSize */
        $storageSize = $request->has('storage')
            ? (int)$request->get('storage')
            : 24064;

        /** @var string $location */
        $location = $request->has('location') ? explode(',', $request->get('location')) : ['AmsterdamAMS-01',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6'
        ];

        $products = product::select(
            'products.name',
            'locations.name as location',
            'memories.name as memory',
            'storages.name as storage',
            'products.currency',
            'products.price'
        )
            ->leftJoin('memories', 'products.memory_id', '=', 'memories.id')
            ->leftJoin('locations', 'products.location_id', '=', 'locations.id')
            ->leftJoin('storages', 'products.storage_id', '=', 'storages.id')
            ->whereIn('memories.size_gigs', $memory)
            ->whereIn('storages.type', $storageType)
            ->whereIn('locations.id', $location)
            ->whereBetween('storages.size_gigs', [0, $storageSize])
            ->get();

        return response()->json($products);
    }

}
