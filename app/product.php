<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    protected $hidden = [
        'memory_id',
        'storage_id',
        'location_id'
    ];

    public function location()
    {
        $this->hasOne(location::class);
    }

    public function memory()
    {
        $this->hasOne(memory::class);
    }

    public function storage()
    {
        $this->hasOne(storage::class);
    }
}
