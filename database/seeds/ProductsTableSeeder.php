<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen(getcwd().'/database/seeds/products/products.csv', "r");
        $header = true;

        while ($csvLine = fgetcsv($handle, 1000, ",")) {

            if ($header) {
                $header = false;
            } else {
                DB::table('products')->insert([
                    'name'          => $csvLine[0],
                    'memory_id'     => $csvLine[1],
                    'storage_id'    => $csvLine[2],
                    'location_id'   => $csvLine[3],
                    'price'         => $csvLine[4],
                    'currency'      => $csvLine[5]
                ]);
            }
        }
    }
}
