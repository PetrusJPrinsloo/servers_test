<?php

use Illuminate\Database\Seeder;

class StoragesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen(getcwd().'/database/seeds/products/storages.csv', "r");
        $header = true;

        while ($csvLine = fgetcsv($handle, 1000, ",")) {

            if ($header) {
                $header = false;
            } else {
                DB::table('storages')->insert([
                    'name' => $csvLine[1],
                    'type' => $csvLine[2],
                    'size_gigs' => $csvLine[3]
                ]);
            }
        }
    }
}
