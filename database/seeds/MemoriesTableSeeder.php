<?php

use Illuminate\Database\Seeder;

class MemoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen(getcwd().'/database/seeds/products/memories.csv', "r");
        $header = true;

        while ($csvLine = fgetcsv($handle, 1000, ",")) {

            if ($header) {
                $header = false;
            } else {
                DB::table('memories')->insert([
                    'name' => $csvLine[1],
                    'size_gigs' => $csvLine[2]
                ]);
            }
        }
    }
}
