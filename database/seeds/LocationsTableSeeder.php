<?php

use Illuminate\Database\Seeder;

class LocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $handle = fopen(getcwd().'/database/seeds/products/locations.csv', "r");
        $header = true;

        while ($csvLine = fgetcsv($handle, 1000, ",")) {

            if ($header) {
                $header = false;
            } else {
                DB::table('locations')->insert([
                    'name' => $csvLine[1]
                ]);
            }
        }
    }
}
